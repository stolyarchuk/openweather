# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-09 14:39-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: src/extension.js:171
msgid "..."
msgstr ""

#: src/extension.js:323
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""

#: src/extension.js:434 src/extension.js:446
#, javascript-format
msgid "Can not connect to %s"
msgstr ""

#: src/extension.js:737 src/weather-settings.ui:302
msgid "Locations"
msgstr ""

#: src/extension.js:741
msgid "Reload Weather Information"
msgstr ""

#: src/extension.js:745
msgid "Weather data by:"
msgstr ""

#: src/extension.js:748
msgid "Weather Settings"
msgstr ""

#: src/extension.js:771
#, javascript-format
msgid "Can not open %s"
msgstr ""

#: src/extension.js:819 src/prefs.js:1072
msgid "Invalid city"
msgstr ""

#: src/extension.js:830
msgid "Invalid location! Please try to recreate it."
msgstr ""

#: src/extension.js:870 src/weather-settings.ui:574
msgid "°F"
msgstr ""

#: src/extension.js:872 src/weather-settings.ui:575
msgid "K"
msgstr ""

#: src/extension.js:874 src/weather-settings.ui:576
msgid "°Ra"
msgstr ""

#: src/extension.js:876 src/weather-settings.ui:577
msgid "°Ré"
msgstr ""

#: src/extension.js:878 src/weather-settings.ui:578
msgid "°Rø"
msgstr ""

#: src/extension.js:880 src/weather-settings.ui:579
msgid "°De"
msgstr ""

#: src/extension.js:882 src/weather-settings.ui:580
msgid "°N"
msgstr ""

#: src/extension.js:884 src/weather-settings.ui:573
msgid "°C"
msgstr ""

#: src/extension.js:921
msgid "Calm"
msgstr ""

#: src/extension.js:924
msgid "Light air"
msgstr ""

#: src/extension.js:927
msgid "Light breeze"
msgstr ""

#: src/extension.js:930
msgid "Gentle breeze"
msgstr ""

#: src/extension.js:933
msgid "Moderate breeze"
msgstr ""

#: src/extension.js:936
msgid "Fresh breeze"
msgstr ""

#: src/extension.js:939
msgid "Strong breeze"
msgstr ""

#: src/extension.js:942
msgid "Moderate gale"
msgstr ""

#: src/extension.js:945
msgid "Fresh gale"
msgstr ""

#: src/extension.js:948
msgid "Strong gale"
msgstr ""

#: src/extension.js:951
msgid "Storm"
msgstr ""

#: src/extension.js:954
msgid "Violent storm"
msgstr ""

#: src/extension.js:957
msgid "Hurricane"
msgstr ""

#: src/extension.js:961
msgid "Sunday"
msgstr ""

#: src/extension.js:961
msgid "Monday"
msgstr ""

#: src/extension.js:961
msgid "Tuesday"
msgstr ""

#: src/extension.js:961
msgid "Wednesday"
msgstr ""

#: src/extension.js:961
msgid "Thursday"
msgstr ""

#: src/extension.js:961
msgid "Friday"
msgstr ""

#: src/extension.js:961
msgid "Saturday"
msgstr ""

#: src/extension.js:967
msgid "N"
msgstr ""

#: src/extension.js:967
msgid "NE"
msgstr ""

#: src/extension.js:967
msgid "E"
msgstr ""

#: src/extension.js:967
msgid "SE"
msgstr ""

#: src/extension.js:967
msgid "S"
msgstr ""

#: src/extension.js:967
msgid "SW"
msgstr ""

#: src/extension.js:967
msgid "W"
msgstr ""

#: src/extension.js:967
msgid "NW"
msgstr ""

#: src/extension.js:1020 src/extension.js:1029 src/weather-settings.ui:607
msgid "hPa"
msgstr ""

#: src/extension.js:1024 src/weather-settings.ui:608
msgid "inHg"
msgstr ""

#: src/extension.js:1034 src/weather-settings.ui:609
msgid "bar"
msgstr ""

#: src/extension.js:1039 src/weather-settings.ui:610
msgid "Pa"
msgstr ""

#: src/extension.js:1044 src/weather-settings.ui:611
msgid "kPa"
msgstr ""

#: src/extension.js:1049 src/weather-settings.ui:612
msgid "atm"
msgstr ""

#: src/extension.js:1054 src/weather-settings.ui:613
msgid "at"
msgstr ""

#: src/extension.js:1059 src/weather-settings.ui:614
msgid "Torr"
msgstr ""

#: src/extension.js:1064 src/weather-settings.ui:615
msgid "psi"
msgstr ""

#: src/extension.js:1069 src/weather-settings.ui:616
msgid "mmHg"
msgstr ""

#: src/extension.js:1074 src/weather-settings.ui:617
msgid "mbar"
msgstr ""

#: src/extension.js:1118 src/weather-settings.ui:593
msgid "m/s"
msgstr ""

#: src/extension.js:1122 src/weather-settings.ui:592
msgid "mph"
msgstr ""

#: src/extension.js:1127 src/weather-settings.ui:591
msgid "km/h"
msgstr ""

#: src/extension.js:1136 src/weather-settings.ui:594
msgid "kn"
msgstr ""

#: src/extension.js:1141 src/weather-settings.ui:595
msgid "ft/s"
msgstr ""

#: src/extension.js:1228
msgid "Loading ..."
msgstr ""

#: src/extension.js:1232
msgid "Please wait"
msgstr ""

#: src/extension.js:1303
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1307
msgid "Humidity:"
msgstr ""

#: src/extension.js:1311
msgid "Pressure:"
msgstr ""

#: src/extension.js:1315
msgid "Wind:"
msgstr ""

#: src/extension.js:1319
msgid "Gusts:"
msgstr ""

#: src/extension.js:1450
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1452
msgid "Day Forecast"
msgstr ""

#: src/openweathermap_org.js:130
msgid "Thunderstorm with light rain"
msgstr ""

#: src/openweathermap_org.js:132
msgid "Thunderstorm with rain"
msgstr ""

#: src/openweathermap_org.js:134
msgid "Thunderstorm with heavy rain"
msgstr ""

#: src/openweathermap_org.js:136
msgid "Light thunderstorm"
msgstr ""

#: src/openweathermap_org.js:138
msgid "Thunderstorm"
msgstr ""

#: src/openweathermap_org.js:140
msgid "Heavy thunderstorm"
msgstr ""

#: src/openweathermap_org.js:142
msgid "Ragged thunderstorm"
msgstr ""

#: src/openweathermap_org.js:144
msgid "Thunderstorm with light drizzle"
msgstr ""

#: src/openweathermap_org.js:146
msgid "Thunderstorm with drizzle"
msgstr ""

#: src/openweathermap_org.js:148
msgid "Thunderstorm with heavy drizzle"
msgstr ""

#: src/openweathermap_org.js:150
msgid "Light intensity drizzle"
msgstr ""

#: src/openweathermap_org.js:152
msgid "Drizzle"
msgstr ""

#: src/openweathermap_org.js:154
msgid "Heavy intensity drizzle"
msgstr ""

#: src/openweathermap_org.js:156
msgid "Light intensity drizzle rain"
msgstr ""

#: src/openweathermap_org.js:158
msgid "Drizzle rain"
msgstr ""

#: src/openweathermap_org.js:160
msgid "Heavy intensity drizzle rain"
msgstr ""

#: src/openweathermap_org.js:162
msgid "Shower rain and drizzle"
msgstr ""

#: src/openweathermap_org.js:164
msgid "Heavy shower rain and drizzle"
msgstr ""

#: src/openweathermap_org.js:166
msgid "Shower drizzle"
msgstr ""

#: src/openweathermap_org.js:168
msgid "Light rain"
msgstr ""

#: src/openweathermap_org.js:170
msgid "Moderate rain"
msgstr ""

#: src/openweathermap_org.js:172
msgid "Heavy intensity rain"
msgstr ""

#: src/openweathermap_org.js:174
msgid "Very heavy rain"
msgstr ""

#: src/openweathermap_org.js:176
msgid "Extreme rain"
msgstr ""

#: src/openweathermap_org.js:178
msgid "Freezing rain"
msgstr ""

#: src/openweathermap_org.js:180
msgid "Light intensity shower rain"
msgstr ""

#: src/openweathermap_org.js:182
msgid "Shower rain"
msgstr ""

#: src/openweathermap_org.js:184
msgid "Heavy intensity shower rain"
msgstr ""

#: src/openweathermap_org.js:186
msgid "Ragged shower rain"
msgstr ""

#: src/openweathermap_org.js:188
msgid "Light snow"
msgstr ""

#: src/openweathermap_org.js:190
msgid "Snow"
msgstr ""

#: src/openweathermap_org.js:192
msgid "Heavy snow"
msgstr ""

#: src/openweathermap_org.js:194
msgid "Sleet"
msgstr ""

#: src/openweathermap_org.js:196
msgid "Shower sleet"
msgstr ""

#: src/openweathermap_org.js:198
msgid "Light rain and snow"
msgstr ""

#: src/openweathermap_org.js:200
msgid "Rain and snow"
msgstr ""

#: src/openweathermap_org.js:202
msgid "Light shower snow"
msgstr ""

#: src/openweathermap_org.js:204
msgid "Shower snow"
msgstr ""

#: src/openweathermap_org.js:206
msgid "Heavy shower snow"
msgstr ""

#: src/openweathermap_org.js:208
msgid "Mist"
msgstr ""

#: src/openweathermap_org.js:210
msgid "Smoke"
msgstr ""

#: src/openweathermap_org.js:212
msgid "Haze"
msgstr ""

#: src/openweathermap_org.js:214
msgid "Sand/Dust Whirls"
msgstr ""

#: src/openweathermap_org.js:216
msgid "Fog"
msgstr ""

#: src/openweathermap_org.js:218
msgid "Sand"
msgstr ""

#: src/openweathermap_org.js:220
msgid "Dust"
msgstr ""

#: src/openweathermap_org.js:222
msgid "VOLCANIC ASH"
msgstr ""

#: src/openweathermap_org.js:224
msgid "SQUALLS"
msgstr ""

#: src/openweathermap_org.js:226
msgid "TORNADO"
msgstr ""

#: src/openweathermap_org.js:228
msgid "Sky is clear"
msgstr ""

#: src/openweathermap_org.js:230
msgid "Few clouds"
msgstr ""

#: src/openweathermap_org.js:232
msgid "Scattered clouds"
msgstr ""

#: src/openweathermap_org.js:234
msgid "Broken clouds"
msgstr ""

#: src/openweathermap_org.js:236
msgid "Overcast clouds"
msgstr ""

#: src/openweathermap_org.js:238
msgid "Not available"
msgstr ""

#: src/openweathermap_org.js:421
msgid "Yesterday"
msgstr ""

#: src/openweathermap_org.js:423
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] ""
msgstr[1] ""

#: src/openweathermap_org.js:438 src/openweathermap_org.js:440
msgid ", "
msgstr ""

#: src/openweathermap_org.js:456
msgid "?"
msgstr ""

#: src/openweathermap_org.js:523
msgid "Tomorrow"
msgstr ""

#: src/prefs.js:189
msgid "Searching ..."
msgstr ""

#: src/prefs.js:201 src/prefs.js:239 src/prefs.js:270 src/prefs.js:274
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr ""

#: src/prefs.js:206 src/prefs.js:246 src/prefs.js:277
#, javascript-format
msgid "\"%s\" not found"
msgstr ""

#: src/prefs.js:224
msgid "You need an AppKey to search on openmapquest."
msgstr ""

#: src/prefs.js:225
msgid "Please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:240
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:241
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:323
msgid "Location"
msgstr ""

#: src/prefs.js:334
msgid "Provider"
msgstr ""

#: src/prefs.js:344
msgid "Result"
msgstr ""

#: src/prefs.js:548
#, javascript-format
msgid "Remove %s ?"
msgstr ""

#: src/prefs.js:561
msgid "No"
msgstr ""

#: src/prefs.js:562
msgid "Yes"
msgstr ""

#: src/prefs.js:1105
msgid "default"
msgstr ""

#: src/weather-settings.ui:29
msgid "Edit name"
msgstr ""

#: src/weather-settings.ui:40 src/weather-settings.ui:56
#: src/weather-settings.ui:183
msgid "Clear entry"
msgstr ""

#: src/weather-settings.ui:47
msgid "Edit coordinates"
msgstr ""

#: src/weather-settings.ui:80 src/weather-settings.ui:216
msgid "Cancel"
msgstr ""

#: src/weather-settings.ui:90 src/weather-settings.ui:226
msgid "Save"
msgstr ""

#: src/weather-settings.ui:124
msgid "Search Results"
msgstr ""

#: src/weather-settings.ui:161
msgid "Search By Location Or Coordinates"
msgstr ""

#: src/weather-settings.ui:184
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr ""

#: src/weather-settings.ui:189
msgid "Find"
msgstr ""

#: src/weather-settings.ui:328
msgid "Choose default weather provider"
msgstr ""

#: src/weather-settings.ui:339
msgid "Personal Api key from openweathermap.org"
msgstr ""

#: src/weather-settings.ui:371
msgid "Refresh timeout for current weather [min]"
msgstr ""

#: src/weather-settings.ui:383
msgid "Refresh timeout for weather forecast [min]"
msgstr ""

#: src/weather-settings.ui:428
msgid "Use extensions api-key for openweathermap.org"
msgstr ""

#: src/weather-settings.ui:437
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""

#: src/weather-settings.ui:451
msgid "Weather provider"
msgstr ""

#: src/weather-settings.ui:475
msgid "Choose geolocation provider"
msgstr ""

#: src/weather-settings.ui:497
msgid "Personal AppKey from developer.mapquest.com"
msgstr ""

#: src/weather-settings.ui:521
msgid "Geolocation provider"
msgstr ""

#: src/weather-settings.ui:545
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr ""

#: src/weather-settings.ui:554
msgid "Wind Speed Unit"
msgstr ""

#: src/weather-settings.ui:563
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
msgid "Pressure Unit"
msgstr ""

#: src/weather-settings.ui:596
msgid "Beaufort"
msgstr ""

#: src/weather-settings.ui:631
msgid "Units"
msgstr ""

#: src/weather-settings.ui:655
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr ""

#: src/weather-settings.ui:664
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr ""

#: src/weather-settings.ui:673
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr ""

#: src/weather-settings.ui:682
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr ""

#: src/weather-settings.ui:691
msgid "Use System Icon Theme"
msgstr ""

#: src/weather-settings.ui:700
msgid "Text on buttons"
msgstr ""

#: src/weather-settings.ui:709
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr ""

#: src/weather-settings.ui:718
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr ""

#: src/weather-settings.ui:727
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/weather-settings.ui:736
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr ""

#: src/weather-settings.ui:745
msgid "Center forecast"
msgstr ""

#: src/weather-settings.ui:754
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Number of days in forecast"
msgstr ""

#: src/weather-settings.ui:763
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Maximal number of digits after the decimal point"
msgstr ""

#: src/weather-settings.ui:777
msgid "Center"
msgstr ""

#: src/weather-settings.ui:778
msgid "Right"
msgstr ""

#: src/weather-settings.ui:779
msgid "Left"
msgstr ""

#: src/weather-settings.ui:926
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: src/weather-settings.ui:950
msgid "Layout"
msgstr ""

#: src/weather-settings.ui:985
msgid "Version: "
msgstr ""

#: src/weather-settings.ui:999
msgid ""
"<span>Display weather information for any location on Earth in the GNOME "
"Shell</span>"
msgstr ""

#: src/weather-settings.ui:1013
msgid ""
"Weather data provided by: <a href=\"https://openweathermap.org/"
"\">OpenWeatherMap</a>"
msgstr ""

#: src/weather-settings.ui:1026
msgid "Maintained by:"
msgstr ""

#: src/weather-settings.ui:1061
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""

#: src/weather-settings.ui:1072
msgid "About"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
msgid "Weather Provider"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
msgid "Geolocation Provider"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
msgid "Wind Speed Units"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
msgid "System Icons"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "Use text on buttons in menu"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
msgid "Refresh interval (forecast)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
msgid "Center forecastbox."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
msgid "Your personal API key from openweathermap.org"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
msgid "Use the extensions default API key from openweathermap.org"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
msgid "Your personal AppKey from developer.mapquest.com"
msgstr ""
